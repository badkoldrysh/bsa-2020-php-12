var vm = new Vue({
    el: '#app',

    data: {
        users: [],
        avatarVars: {
            topType: ["ShortHairFrizzle", "LongHairCurly", "ShortHairShortFlat", "WinterHat1"],
            hairColor: ["Black", "Blonde", "Red", "BlondeGolden"],
            facialHairType: ["Blank", "BeardLight", "BeardMagestic", "MoustacheMagnum"],
            clotheType: ["Overall", "ShirtVNeck", "Hoodie", "CollarSweater"],
            eyeType: ["Wink", "Happy", "Default", "Side"],
            mouthType: ["Twinkle", "Smile", "Default", "Sad"],
            skinColor: ["Yellow", "Black", "Light", "Brown"],
        },
        currentUser: {
            id: null,
            name: null,
            email: null,
            avatar: null
        },
        nextId: 0,
        searchLine: '',
        searchByName: true,
    },

    computed: {
        foundUsers: function() {
            if (this.searchLine !== '') {
                const searchLine = this.searchLine.toLowerCase();
                const searchByName = this.searchByName;
                return this.users.filter(function (user) {
                    const searchIn = searchByName
                        ? user.name.toLowerCase()
                        : user.email.toLowerCase();
                    return !(searchIn.indexOf(searchLine) < 0);
                });
            }
            return [];
        }
    },

    methods: {
        addUser: function() {
            this.currentUser.id = this.nextId++;
            for (let property in this.currentUser) {
                if (this.currentUser[property] === null) {
                    return;
                }
            }
            this.users.push(this.currentUser);
            this.resetCurrentUser();
        },
        deleteCurrentUser: function() {
            if (this.currentUser.id !== null) {
                const currentUserId = this.currentUser.id;
                this.users = this.users.filter(function(user) {
                    if (user.id !== currentUserId) {
                        return user;
                    }
                });

                this.resetCurrentUser();
            }
        },
        resetCurrentUser: function() {
            this.currentUser = {
                id: null,
                name: null,
                email: null,
                avatar: null
            };
        },
        setCurrentUser: function(user) {
            this.currentUser = user;
        },
        switchSearch: function() {
            this.searchByName = this.searchByName ? false : true;
            this.searchLine = '';
        },
        getRand: function () {
            return Math.floor(Math.random() * 4);
        },
        getRandomAvatar: function () {
            const base = "https://avataaars.io/?avatarStyle=Transparent";
            const avatarGen = '&topType=' + this.avatarVars.topType[this.getRand()]
                        + '&accessoriesType=Blank'
                        + '&hairColor=' + this.avatarVars.hairColor[this.getRand()]
                        + '&facialHairType=' + this.avatarVars.facialHairType[this.getRand()]
                        + '&facialHairColor=Black'
                        + '&clotheType=' + this.avatarVars.clotheType[this.getRand()]
                        + '&clotheColor=Blue03'
                        + '&eyeType=' + this.avatarVars.eyeType[this.getRand()]
                        + '&eyebrowType=UpDown'
                        + '&mouthType=' + this.avatarVars.mouthType[this.getRand()]
                        + '&skinColor=' + this.avatarVars.skinColor[this.getRand()];

            return base + avatarGen;
        }
    },
    created() {
        axios
            .get('https://jsonplaceholder.typicode.com/users')
            .then(response => {
                for (user of response.data) {
                    this.users.push({
                        id: user.id,
                        name: user.name,
                        email: user.email,
                        avatar: this.getRandomAvatar()
                    });
                    this.nextId = this.users.length + 1;
                }
            });

    }
});
